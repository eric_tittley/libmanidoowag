#include "Manidoowag.h"

extern "C" {
/* RT includes */
#include "RT.h"
}

void findNextTimestep(size_t const nParticles,
                      rt_float const t_stop,
                      rt_float const t,
                      RT_Data const Data_dev,
                      RT_RatesBlockT const Rates_dev,
                      RT_ConstantsT const Constants,
                      /* Output */
                      rt_float &dt_RT,
                      unsigned char &LastIterationFlag) {
  RT_TimeScale dt_RT_sugg;
  /* Reset the timesteps */
  RT_TimeScale_SetInfiniteTime(&dt_RT_sugg);

  /* Only "Entropy" is used, and it fills in for all timescales searched in
     findMinimumTimescale(); it is the minimum of all searched timscales */
  /* Don't necessarily need the device copy of Constants
   * since it is copied to the kernel and no pointers are accessed. */
  dt_RT_sugg.Entropy = findMinimumTimescale(Constants, Rates_dev, Data_dev);

  /* The appropriate time step */
  RT_NextTimeStep(
      t_stop, t, Constants, &dt_RT_sugg, &dt_RT, &LastIterationFlag);

  if (!LastIterationFlag) {
    if (dt_RT < Constants.MIN_DT) dt_RT = Constants.MIN_DT;
  }
  if (dt_RT > Constants.MAX_DT) {
    dt_RT = Constants.MAX_DT;
    /* Since shrinking dt_RT, this can't be the last iteration */
    LastIterationFlag = FALSE;
  }
  return;
}
