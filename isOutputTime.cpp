#include <limits>

#include "Manidoowag.h"

bool isOutputTime(rt_float const t_outputTimesUnit,
                  rt_float const* const OutputTimes,
                  size_t const nOutputTimes,
                  size_t const OutputTimeIndex) {
  if (OutputTimeIndex < nOutputTimes &&
      ((t_outputTimesUnit / OutputTimes[OutputTimeIndex]) >=
       (1.0 - std::numeric_limits<rt_float>::epsilon())))
    return true;
  else
    return false;
}
