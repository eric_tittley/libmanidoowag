#include <stdio.h>

#include "Manidoowag.h"

static int read_float(FILE *FID, rt_float *value) {
  int nRead;
  nRead = fscanf(FID, FLOATSTR, value);
  return nRead;
}

size_t readOutputTimes(
    char const *const OutputTimesFile,
    rt_float &outputTimesUnit, /* To convert outputTimes[i] to s */
    rt_float *&outputTimes) {
  int nRead;
  size_t nTimes;
  rt_float dummy;

  FILE *FID = fopen(OutputTimesFile, "r");
  if (FID == NULL) return 0;

  nRead = fscanf(FID, "%lu", &nTimes);
  if (nRead == EOF) return 0;

  nRead = read_float(FID, &outputTimesUnit);
  if (nRead == EOF) return 0;

  outputTimes = (rt_float *)malloc(sizeof(rt_float) * nTimes);
  if (outputTimes == NULL) return 0;

  for (size_t iTime = 0; iTime < nTimes; ++iTime) {
    nRead = read_float(FID, outputTimes + iTime); /* Pointer arithmetic */
    if (nRead != 1) {
      free(outputTimes);
      outputTimes = NULL;
      return 0;
    }
  }

  /* There should be no more values to read. */
  nRead = read_float(FID, &dummy);
  if (nRead != EOF) {
    free(outputTimes);
    outputTimes = NULL;
    return 0;
  }

  return nTimes;
}
