#include "Manidoowag.h"
#include "cudasupport.h"

// Heating
__global__ void uCHR_kernel(RT_RatesBlockT Rates) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t iParticle = istart; iParticle < Rates.NumCells;
       iParticle += span) {
    Rates.G[iParticle] = Rates.G_H1[iParticle];
    Rates.G[iParticle] += Rates.G_He1[iParticle];
    Rates.G[iParticle] += Rates.G_He2[iParticle];
  }
}

void updateCumulativeHeatingRate(RT_RatesBlockT Rates) {
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(
      Rates.NumCells, (void *)uCHR_kernel, &gridSize, &blockSize);

  uCHR_kernel<<<gridSize, blockSize>>>(Rates);
  bool const TerminateOnCudaError = true;
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
}
