#include <stdio.h>

#include "Manidoowag.h"

size_t readHaloCatalogue(char *const HaloCatalogueFile,
                         rt_float *&haloCentres,
                         rt_float *&Ls) {
  FILE *FID = fopen(HaloCatalogueFile, "r");

  if (FID == NULL) return 0;

  int nRead;

  size_t nHalos;
  nRead = fscanf(FID, "%lu", &nHalos);
  if (nRead == EOF) return 0;

  haloCentres = (rt_float *)malloc(sizeof(rt_float) * nHalos * 3);
  if (haloCentres == NULL) return 0;

  Ls = (rt_float *)malloc(sizeof(rt_float) * nHalos);
  if (Ls == NULL) return 0;

  for (size_t iHalo = 0; iHalo < nHalos; ++iHalo) {
    for (size_t iDim = 0; iDim < 3; ++iDim) {
      nRead = fscanf(FID,
                     FLOATSTR,
                     haloCentres + 3 * iHalo + iDim); /* Pointer arithmetic */
      if (nRead != 1) return 0;
    }
    nRead = fscanf(FID, FLOATSTR, Ls + iHalo); /* Pointer arithmetic */
    if (nRead != 1) return 0;
  }

  return nHalos;
}
