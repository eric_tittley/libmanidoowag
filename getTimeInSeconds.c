/* getTimeInSeconds: Returns the number of seconds (to microsecond precision)
 * since the Epoch 1970-01-01 00:00:00 +0000 (UTC) */

#include <stdlib.h>
#include <sys/time.h>

#include "Manidoowag.h"

double getTimeInSeconds() {
  struct timeval now;
  gettimeofday(&now, NULL);
  return (double)now.tv_usec / 1000000.0 + (double)now.tv_sec;
}
