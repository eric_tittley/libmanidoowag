#include "Manidoowag.h"
#include "cudasupport.h"

__global__ void initVector_kernel(rt_float *const A,
                                  rt_float const V,
                                  size_t const nElements) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock
  for (size_t cellindex = istart; cellindex < nElements; cellindex += span) {
    A[cellindex] = V;
  }
}

void setVectorToValue(rt_float *const Vector,
                      rt_float const Value,
                      size_t const count) {
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size

  setGridAndBlockSize(count, (void *)initVector_kernel, &gridSize, &blockSize);

  initVector_kernel<<<gridSize, blockSize>>>(Vector, Value, count);

  bool const TerminateOnCudaError = true;
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
}
