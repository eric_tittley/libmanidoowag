#include <cstdlib>

#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void cIR_kernel(RT_ConstantsT const Constants,
                           rt_float const alpha_H1,
                           rt_float const alpha_He1,
                           rt_float const alpha_He2,
                           RT_Data const Data,
                           RT_RatesBlockT Rates) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data.NumCells; cellindex += span) {
    RT_CUDA_CollisionalIonizationRate(&Constants, cellindex, alpha_H1, alpha_He1, alpha_He2, &Data, &Rates);
  }
}

void collisionalIonizationRate(RT_ConstantsT const Constants,
                               RT_Data Data_dev,
                               /*Updated*/ RT_RatesBlockT const Rates_dev) {
  rt_float const alpha_H1 = H1_H2(Constants.nu_0, Constants);
  rt_float const alpha_He1 = He1_He2(Constants.nu_1, Constants);
  rt_float const alpha_He2 = He2_He3(Constants.nu_2, Constants);
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(Data_dev.NumCells, (void*)cIR_kernel, &gridSize, &blockSize);

  cIR_kernel<<<gridSize, blockSize>>>(Constants, alpha_H1, alpha_He1, alpha_He2, Data_dev, Rates_dev);

  bool const TerminateOnCudaError = true;
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
}