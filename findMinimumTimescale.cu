#include <thrust/device_ptr.h>
#include <thrust/extrema.h>

#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void fMT_findTauInEachCell_kernel(
    RT_ConstantsT const Constants,
    RT_Data const Data,
    /* out */ RT_RatesBlockT const Rates,
    /* out */ rt_float *tau_ion,
    rt_float *tau_cool) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data.NumCells;
       cellindex += span) {
    /* Find the neutral & ionised number densities. */
    rt_float const n_H1 = Data.n_H[cellindex] * Data.f_H1[cellindex];
    rt_float const n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];
    rt_float const n_H2 = Data.n_H[cellindex] * Data.f_H2[cellindex];
    rt_float const n_He3 = Data.n_He[cellindex] * Data.f_He3[cellindex];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

#ifndef CONSTANT_TEMPERATURE
    /* Entropy timescale */
    rt_float ResidualHeatingScaleFactorGammaInv =
        POW(Constants.Ma, 1. / Constants.gamma_ad);
    rt_float EntropyRate =
        ((Constants.gamma_ad - 1.) /
         POW(Data.Density[cellindex] * ResidualHeatingScaleFactorGammaInv,
             Constants.gamma_ad) *
         (Rates.G[cellindex] - Rates.L[cellindex]));

#  ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
    /* Particles at minimum temperature are allowed to cool below zero.
     * Otherwise time steps degrade massively. */
    rt_float const Entropy_min = RT_CUDA_EntropyFromTemperature(
        ENFORCE_MINIMUM_CMB_TEMPERATURE, cellindex, &Data, &Constants);
    if (Data.Entropy[cellindex] - Entropy_min < 0.01 * Entropy_min) {
      EntropyRate = std::max(EntropyRate, RT_FLT_C(0.));
    }
#  endif

    /* Only worry about cooling too fast */
    if (EntropyRate < 0.) {
      tau_cool[cellindex] = -Data.Entropy[cellindex] / EntropyRate;
#  ifdef DEBUG
      if (!(bool)isfinite(tau_cool[cellindex]) || tau_cool[cellindex] <= 0.0) {
        printf(
            "ERROR: %s: %i: tau_test = %e; Entropy[%lu]=%5.3e; "
            "EntropyRate=%5.3e\n",
            __FILE__,
            __LINE__,
            tau_cool[cellindex],
            cellindex,
            Data.Entropy[cellindex],
            EntropyRate);
      }
#  endif
    }
#endif

    /* Ionization timescale */
    /* Trigger on dNH1dt increasing or decreasing */
    rt_float const dNH1dt =
        (n_H2 * n_e * Rates.alpha_H1[cellindex] - Rates.I_H1[cellindex]);

    if (!RT_CUDA_useEquilibriumValues(cellindex, &Data, &Rates, &Constants)) {
      rt_float Delta;
      if (n_H1 >= n_H2) {
        Delta = n_H1 - Rates.n_HI_Equilibrium[cellindex];
      } else {
        Delta = n_H2 - Rates.n_HII_Equilibrium[cellindex];
      }
      tau_ion[cellindex] = FABS(Delta / dNH1dt);
      /* Curious state can occur when the gas is already at equilibrium but
       * there is a small residual (probably numeric) dNH1dt */
      if (Delta == 0.0) tau_ion[cellindex] = 1e25;
#ifdef DEBUG
      if (!(bool)isfinite(tau_ion[cellindex]) || tau_ion[cellindex] <= 0.0) {
        printf("ERROR: %s: %i: tau_test = %e; Delta=%5.3e;dNH1dt=%5.3e\n",
               __FILE__,
               __LINE__,
               tau_ion[cellindex],
               Delta,
               dNH1dt);
      }
#endif
    }

    /*** TODO: Add rates for changes to H2, He2, & He3 ***/

    Rates.TimeScale[cellindex] =
        std::min(tau_ion[cellindex], tau_cool[cellindex]);
  }  // end if cellindex < NumCells
}

rt_float findMinimumTimescale(RT_ConstantsT const Constants,
                              RT_RatesBlockT const Rates_dev,
                              RT_Data const Data_dev) {
  /* Don't need the device copy of Constants
   * since it is copied to the kernel and no pointers are accessed. */

  bool const TerminateOnCudaError = true;

  // Allocate and Initialize the timescale for each cell
  rt_float *tau_ion, *tau_cool;
  rt_float const InfiniteTime = 1e20;
  cudaError_t cudaError =
      cudaMalloc((void **)&tau_ion, sizeof(rt_float) * Data_dev.NumCells);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  setVectorToValue(tau_ion, InfiniteTime, Data_dev.NumCells);

  cudaError =
      cudaMalloc((void **)&tau_cool, sizeof(rt_float) * Data_dev.NumCells);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  setVectorToValue(tau_cool, InfiniteTime, Data_dev.NumCells);

  // Find timescale Tau of each cell //
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size

  setGridAndBlockSize(Data_dev.NumCells,
                      (void *)fMT_findTauInEachCell_kernel,
                      &gridSize,
                      &blockSize);

  fMT_findTauInEachCell_kernel<<<gridSize, blockSize>>>(Constants,
                                                        Data_dev,
                                                        /* out */ Rates_dev,
                                                        /* out */ tau_ion,
                                                        tau_cool);
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);

  thrust::device_ptr<float> tau_ion_ptr = thrust::device_pointer_cast(tau_ion);
  thrust::device_ptr<float> tau_cool_ptr =
      thrust::device_pointer_cast(tau_cool);
  thrust::device_ptr<float> tau_ion_min_ptr =
      thrust::min_element(tau_ion_ptr, tau_ion_ptr + Data_dev.NumCells);
  thrust::device_ptr<float> tau_cool_min_ptr =
      thrust::min_element(tau_cool_ptr, tau_cool_ptr + Data_dev.NumCells);

  float tau_ion_min;
  float tau_cool_min;

  unsigned int ion_index = thrust::distance(tau_ion_ptr, tau_ion_min_ptr);
  cudaMemcpy(
      &tau_ion_min, &tau_ion[ion_index], sizeof(float), cudaMemcpyDeviceToHost);

  unsigned int cool_index = thrust::distance(tau_cool_ptr, tau_cool_min_ptr);
  cudaMemcpy(&tau_cool_min,
             &tau_cool[cool_index],
             sizeof(float),
             cudaMemcpyDeviceToHost);

  printf("Min t_ion %e, min t_cool %e\n", tau_ion_min, tau_cool_min);
  unsigned int id = ion_index;
  if (tau_cool_min < tau_ion_min) id = cool_index;
  float f_H1, L, G, Entropy, T, Density, I_H1, a_H1, n_H1_equilib;

  cudaMemcpy(&f_H1, &Data_dev.f_H1[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&T, &Data_dev.T[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&Density, &Data_dev.Density[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&n_H1_equilib, &Rates_dev.n_HI_Equilibrium[id], sizeof(float), cudaMemcpyDeviceToHost);

  cudaMemcpy(
      &Entropy, &Data_dev.Entropy[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&G, &Rates_dev.G[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&L, &Rates_dev.L[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&I_H1, &Rates_dev.I_H1[id], sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(&a_H1, &Rates_dev.alpha_H1[id], sizeof(float), cudaMemcpyDeviceToHost);

  printf("id=%d; f_H1=%8.3e; T=%8.3e; G=%8.3e; L=%8.3e; Density=%8.3e; I_H1=%8.3e, a_H1=%8.3e; n_H1_equilib=%8.3e\n", id, f_H1, T, G, L, Density, I_H1, a_H1,n_H1_equilib);

  cudaFree(tau_ion);
  cudaFree(tau_cool);

  return std::min(tau_ion_min, tau_cool_min);
}
