/* Create multiple sources from a single source, with the luminosity scaled to 
 * conserve total luminosity*/

#include <stddef.h>

#include "RT_SourceT.h"

void MultipleSourcesFromSingle(size_t const nSources, RT_SourceT *Source) {

  /* Source[0] needs to be set. */

  /* Divide Source[0].Luminosity by nSource. */
  Source[0].L_0 = Source[0].L_0/((double)nSources);

  /* Copy Source[0] to the other sources. */
  for (size_t i = 1; i < nSources; ++i) {
    Source[i] = Source[0];
  }
}