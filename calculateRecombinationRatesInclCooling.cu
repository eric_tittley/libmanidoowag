#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void cRRIC_kernel(rt_float const ExpansionFactor,
                             RT_ConstantsT const Constants,
                             RT_Data const Data_dev,
                             /* updated */
                             RT_RatesBlockT Rates_dev) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    RT_CUDA_RecombinationRatesInclCooling(cellindex,
                                          ExpansionFactor,
                                          &Constants,
                                          &Data_dev,
                                          /*@out@*/ &Rates_dev);
  }
}

void calculateRecombinationRatesInclCooling(
    rt_float const ExpansionFactor,
    RT_ConstantsT const Constants_dev,
    RT_Data const Data_dev,
    /* updated */ RT_RatesBlockT Rates_dev) {
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(
      Data_dev.NumCells, (void *)cRRIC_kernel, &gridSize, &blockSize);

  cRRIC_kernel<<<gridSize, blockSize>>>(ExpansionFactor,
                                        Constants_dev,
                                        Data_dev,
                                        /* out */ Rates_dev);
  bool const TerminateOnCudaError = true;
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
}
