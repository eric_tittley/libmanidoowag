/* The contents are a modified version of code posted 2011-10-10
 * by Jimmy Pattersson on devtalk.nvidia.com.
 * https://devtalk.nvidia.com/cmd/default/download-comment-attachment/38562/
 */

#include <cuda.h>
#include <math.h>

#include "Epsilon.h"
#include "Manidoowag.h"
#include "cudasupport.h"

#define inf FLOAT_MAX

const int threads = 64;

__device__ void warp_reduce_max(volatile rt_float smem[64]) {
  smem[threadIdx.x] = smem[threadIdx.x + 32] > smem[threadIdx.x]
                          ? smem[threadIdx.x + 32]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 16] > smem[threadIdx.x]
                          ? smem[threadIdx.x + 16]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 8] > smem[threadIdx.x]
                          ? smem[threadIdx.x + 8]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 4] > smem[threadIdx.x]
                          ? smem[threadIdx.x + 4]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 2] > smem[threadIdx.x]
                          ? smem[threadIdx.x + 2]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 1] > smem[threadIdx.x]
                          ? smem[threadIdx.x + 1]
                          : smem[threadIdx.x];
}

__device__ void warp_reduce_min(volatile rt_float smem[64]) {
  smem[threadIdx.x] = smem[threadIdx.x + 32] < smem[threadIdx.x]
                          ? smem[threadIdx.x + 32]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 16] < smem[threadIdx.x]
                          ? smem[threadIdx.x + 16]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 8] < smem[threadIdx.x]
                          ? smem[threadIdx.x + 8]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 4] < smem[threadIdx.x]
                          ? smem[threadIdx.x + 4]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 2] < smem[threadIdx.x]
                          ? smem[threadIdx.x + 2]
                          : smem[threadIdx.x];
  smem[threadIdx.x] = smem[threadIdx.x + 1] < smem[threadIdx.x]
                          ? smem[threadIdx.x + 1]
                          : smem[threadIdx.x];
}

template <int threads>
__global__ void find_min_max_dynamic(rt_float* in,
                                     rt_float* out,
                                     size_t n,
                                     size_t start_adr,
                                     size_t num_blocks) {
  __shared__ rt_float smem_min[64];
  __shared__ rt_float smem_max[64];

  size_t tid = threadIdx.x + start_adr;

  rt_float max = -inf;
  rt_float min = inf;
  rt_float val;

  // tail part
  size_t mult = 0;

  for (size_t i = 1; mult + tid < n; i++) {
    val = in[tid + mult];

    min = val < min ? val : min;
    max = val > max ? val : max;

    mult = i * threads;
  }

  // previously reduced MIN part
  mult = 0;
  size_t i;
  for (i = 1; mult + threadIdx.x < num_blocks; i++) {
    val = out[threadIdx.x + mult];

    min = val < min ? val : min;

    mult = i * threads;
  }

  // MAX part
  for (; mult + threadIdx.x < num_blocks * 2; i++) {
    val = out[threadIdx.x + mult];

    max = val > max ? val : max;

    mult = i * threads;
  }

  if (threads == 32) {
    smem_min[threadIdx.x + 32] = 0.0f;
    smem_max[threadIdx.x + 32] = 0.0f;
  }

  smem_min[threadIdx.x] = min;
  smem_max[threadIdx.x] = max;

  __syncthreads();

  if (threadIdx.x < 32) {
    warp_reduce_min(smem_min);
    warp_reduce_max(smem_max);
  }
  if (threadIdx.x == 0) {
    out[blockIdx.x] = smem_min[threadIdx.x];  // out[0] == ans
    out[blockIdx.x + gridDim.x] = smem_max[threadIdx.x];
  }
}

template <int els_per_block, int threads>
__global__ void find_min_max(rt_float* in, rt_float* out) {
  __shared__ rt_float smem_min[64];
  __shared__ rt_float smem_max[64];

  size_t tid = threadIdx.x + blockIdx.x * els_per_block;

  rt_float max = -inf;
  rt_float min = inf;
  rt_float val;

  const size_t iters = els_per_block / threads;

#pragma unroll
  for (size_t i = 0; i < iters; i++) {
    val = in[tid + i * (size_t)threads];

    min = val < min ? val : min;
    max = val > max ? val : max;
  }

  if (threads == 32) {
    smem_min[threadIdx.x + 32] = 0.0f;
    smem_max[threadIdx.x + 32] = 0.0f;
  }

  smem_min[threadIdx.x] = min;
  smem_max[threadIdx.x] = max;

  __syncthreads();

  if (threadIdx.x < 32) {
    warp_reduce_min(smem_min);
    warp_reduce_max(smem_max);
  }

  if (threadIdx.x == 0) {
    out[blockIdx.x] = smem_min[threadIdx.x];  // out[0] == ans
    out[blockIdx.x + gridDim.x] = smem_max[threadIdx.x];
  }
}

int findBlockSize(size_t* num_el) {
  int whichSize;
  const float pretty_big_number = 24.0f * 1024.0f * 1024.0f;

  float ratio = float((*num_el)) / pretty_big_number;

  if (ratio > 0.8f)
    whichSize = 5;
  else if (ratio > 0.6f)
    whichSize = 4;
  else if (ratio > 0.4f)
    whichSize = 3;
  else if (ratio > 0.2f)
    whichSize = 2;
  else
    whichSize = 1;

  return whichSize;
}

void compute_reduction(rt_float* d_in, rt_float* d_out, size_t num_els) {
  int const blockSize1 = 2048;

  int const whichSize = findBlockSize(&num_els);

  int const block_size = (int)(powf(2.0, (float)(whichSize - 1))) * blockSize1;
  int const num_blocks = num_els / block_size;
  int const tail = num_els - num_blocks * block_size;
  int const start_adr = num_els - tail;

  if (whichSize == 1)
    find_min_max<blockSize1, threads><<<num_blocks, threads>>>(d_in, d_out);
  else if (whichSize == 2)
    find_min_max<blockSize1 * 2, threads><<<num_blocks, threads>>>(d_in, d_out);
  else if (whichSize == 3)
    find_min_max<blockSize1 * 4, threads><<<num_blocks, threads>>>(d_in, d_out);
  else if (whichSize == 4)
    find_min_max<blockSize1 * 8, threads><<<num_blocks, threads>>>(d_in, d_out);
  else
    find_min_max<blockSize1 * 16, threads>
        <<<num_blocks, threads>>>(d_in, d_out);

  find_min_max_dynamic<threads>
      <<<1, threads>>>(d_in, d_out, num_els, start_adr, num_blocks);
}

void minmaxOfVector(rt_float* Vector_dev,
                    size_t num_els,
                    /* out */
                    rt_float* min,
                    rt_float* max) {
  bool const TerminateOnCudaError = true;

  rt_float* buffer_dev;
  cudaError_t cudaError =
      cudaMalloc((void**)&buffer_dev, sizeof(rt_float) * num_els);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);

  compute_reduction(Vector_dev, buffer_dev, num_els);
  cudaMemcpy(min, buffer_dev, sizeof(rt_float), cudaMemcpyDeviceToHost);
  cudaMemcpy(max, buffer_dev + 1, sizeof(rt_float), cudaMemcpyDeviceToHost);

  cudaFree(buffer_dev);
}
