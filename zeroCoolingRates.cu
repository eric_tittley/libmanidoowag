/* zeroRates: Set rates to zero prior to accumulation.
 *
 * WARNING: Returning, but the asynchronous memsets may still be ongoing.
 *
 * ARGUMENTS
 *  Rates_dev RT_RatesBlockT structure on the host whose elements are pointers
 * to global memory spaces on the device. double *I_H1 double *I_He1 double
 * *I_He2 double *G_H1 double *G_He1 double *G_He2
 *
 * RETURNS
 *  Nothing
 */
 #include "Manidoowag.h"
 #include "cudasupport.h"
 
 void zeroRates(RT_RatesBlockT Rates_dev) {
   size_t const count = sizeof(rt_float) * Rates_dev.NumCells;
 
   bool const TerminateOnCudaError = true;
   cudaError_t cudaError;
 
   cudaError = cudaMemsetAsync((void *)Rates_dev.alpha_H1, 0., count);
   checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
   cudaError = cudaMemsetAsync((void *)Rates_dev.alpha_He1, 0., count);
   checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
   cudaError = cudaMemsetAsync((void *)Rates_dev.alpha_He2, 0., count);
   checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
   cudaError = cudaMemsetAsync((void *)Rates_dev.L, 0., count);
   checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
 
   /* WARNING: Returning, but the memsets may still be ongoing. */
   return;
 }
 