#include "Manidoowag.h"

size_t skipMissedOutputTimes(rt_float const t_outputTimesUnit,
                             rt_float const* const OutputTimes,
                             size_t const nOutputTimes,
                             size_t* const OutputTimeIndex) {
  if (*OutputTimeIndex >= nOutputTimes) return 0;

  size_t skipped = 0;
  while (t_outputTimesUnit >= OutputTimes[*OutputTimeIndex] &&
         *OutputTimeIndex < nOutputTimes) {
    *OutputTimeIndex += 1;
    skipped += 1;
  }
  return skipped;
}
