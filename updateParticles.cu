#include <cstdlib>

#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void uP_kernel(rt_float const dt_RT,
                          rt_float const ExpansionFactor,
                          RT_ConstantsT const Constants,
                          RT_RatesBlockT const Rates,
                          /* out */ RT_Data Data) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data.NumCells;
       cellindex += span) {
    RT_CUDA_UpdateCell_RateBlock(dt_RT,
                                 ExpansionFactor,
                                 cellindex,
                                 &Constants,
                                 &Rates,
                                 /* updated */ &Data);
  }
}

void updateParticles(rt_float const dt_RT,
                     rt_float const Redshift,
                     RT_ConstantsT const Constants,
                     RT_RatesBlockT const Rates_dev,
                     /* Updated */ RT_Data Data_dev) {
  const rt_float ExpansionFactor = 1. / (Redshift + 1.0);

  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(
      Data_dev.NumCells, (void *)uP_kernel, &gridSize, &blockSize);

  uP_kernel<<<gridSize, blockSize>>>(dt_RT,
                                     ExpansionFactor,
                                     Constants,
                                     Rates_dev,
                                     /* out */ Data_dev);

  bool const TerminateOnCudaError = true;
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
}
