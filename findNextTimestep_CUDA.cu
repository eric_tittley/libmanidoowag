
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/extrema.h>

extern "C" {
/* RT includes */
#include "RT.h"
}

#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

#define MAX_COOLING_LOOPS 32

__global__ void fMT_findTauCool_kernel(RT_ConstantsT const Constants,
                                       RT_Data const Data,
                                       RT_RatesBlockT const Rates,
                                       rt_float *tau_cool) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data.NumCells;
       cellindex += span) {
#ifndef CONSTANT_TEMPERATURE
    /* Entropy timescale */
    rt_float ResidualHeatingScaleFactorGammaInv =
        POW(Constants.Ma, 1. / Constants.gamma_ad);
    rt_float EntropyRate =
        ((Constants.gamma_ad - 1.) /
         POW(Data.Density[cellindex] * ResidualHeatingScaleFactorGammaInv,
             Constants.gamma_ad) *
         (Rates.G[cellindex] - Rates.L[cellindex]));

#  ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
    /* Particles at minimum temperature are allowed to cool below zero.
     * Otherwise time steps degrade massively. */
    rt_float const Entropy_min = RT_CUDA_EntropyFromTemperature(
        ENFORCE_MINIMUM_CMB_TEMPERATURE, cellindex, &Data, &Constants);
    if (Data.Entropy[cellindex] - Entropy_min < 0.01 * Entropy_min) {
      EntropyRate = std::max(EntropyRate, RT_FLT_C(0.));
    }
#  endif

    if( std::abs(EntropyRate) > 0.0 ) {
      tau_cool[cellindex] =
          std::abs(Data.Entropy[cellindex] / EntropyRate) * 0.05;
#  ifdef DEBUG
      if (!(bool)isfinite(tau_cool[cellindex]) || tau_cool[cellindex] <= 0.0) {
        printf(
            "ERROR: %s: %i: tau_cool = %e; Entropy[%lu]=%5.3e; "
            "EntropyRate=%5.3e;G=%5.3e;L=%5.3e\n",
            __FILE__,
            __LINE__,
            tau_cool[cellindex],
            cellindex,
            Data.Entropy[cellindex],
            EntropyRate,
            Rates.G[cellindex],
            Rates.L[cellindex]);
      }
#  endif
}

#endif  // Constant Temperature
  }     // end if cellindex < NumCells
}

__global__ void fMT_findTauIon_kernel(RT_ConstantsT const Constants,
                                      RT_Data const Data,
                                      RT_RatesBlockT const Rates,
                                      rt_float *tau_ion) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data.NumCells;
       cellindex += span) {
    /* Find the neutral & ionised number densities. */
    rt_float const n_H1 = Data.n_H[cellindex] * Data.f_H1[cellindex];
    rt_float const n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];
    rt_float const n_H2 = Data.n_H[cellindex] * Data.f_H2[cellindex];
    rt_float const n_He3 = Data.n_He[cellindex] * Data.f_He3[cellindex];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

    /* Ionization timescale */
    /* Trigger on dNH1dt increasing or decreasing */
    rt_float const dNH1dt =
        (n_H2 * n_e * Rates.alpha_H1[cellindex] - Rates.I_H1[cellindex]);

    if (!RT_CUDA_useEquilibriumValues(cellindex, &Data, &Rates, &Constants)) {
      rt_float Delta;
      /*
            if (n_H1 >= n_H2) {
              Delta = n_H1 - Rates.n_HI_Equilibrium[cellindex];
            } else {
              Delta = n_H2 - Rates.n_HII_Equilibrium[cellindex];
            }
            */
      if (dNH1dt > 0)
        Delta = n_H2;
      else
        Delta = n_H1;
      tau_ion[cellindex] = FABS(Delta / dNH1dt);
      /* Curious state can occur when the gas is already at equilibrium but
       * there is a small residual (probably numeric) dNH1dt */
      if (Delta == 0.0) tau_ion[cellindex] = 1e25;
#ifdef DEBUG
      if (!(bool)isfinite(tau_ion[cellindex]) || tau_ion[cellindex] <= 0.0) {
        printf("ERROR: %s: %i: tau_ion = %e; Delta=%5.3e;dNH1dt=%5.3e\n",
               __FILE__,
               __LINE__,
               tau_ion[cellindex],
               Delta,
               dNH1dt);
      }
#endif
    }
  }  // end if cellindex < NumCells
}

template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y, int ulp = 4) {
  // the machine epsilon has to be scaled to the magnitude of the values used
  // and multiplied by the desired precision in ULPs (units in the last place)
  return std::fabs(x - y) <=
             std::numeric_limits<T>::epsilon() * std::fabs(x + y) * ulp
         // unless the result is subnormal
         || std::fabs(x - y) < std::numeric_limits<T>::min();
}

void imposeDtLimits(RT_ConstantsT Constants, rt_time_t *time) {
  /* Limits on dt_RT */
  if (time->dt_RT < Constants.MIN_DT)
    time->dt_RT =
        Constants.MIN_DT;  // this should probably be a crash condition
  if (time->dt_RT > Constants.MAX_DT) time->dt_RT = Constants.MAX_DT;

  double predicted_time = time->current_time + time->dt_RT;
  /* t_end is a hard stop */
  if (predicted_time > time->t_end ||
      almost_equal(predicted_time, time->t_end)) {
    time->dt_RT = time->t_end - time->current_time;
    predicted_time = time->current_time + time->dt_RT;
    time->LastIterationFlag = true;
    time->OutputFlag = true;
    printf("Last Iteration Flag set\n");
  }

  /* Check if we need to output this timestep */
  if (predicted_time >= time->nextOutputTime ||
      almost_equal(predicted_time, time->nextOutputTime)) {
    time->dt_RT = time->nextOutputTime - time->current_time;
    time->OutputFlag = true;
  }

  if (time->dt_RT <
      std::numeric_limits<double>::epsilon() * time->current_time * 4) {
    printf("ERROR:dt_RT is within 4UPS of numeric limits!!!\n");
  }
}

void findNextTimestep_CUDA(size_t const nParticles,
                           RT_Data const Data_dev,
                           RT_RatesBlockT const Rates_dev,
                           RT_ConstantsT const Constants,
                           rt_time_t *time) {
  bool const TerminateOnCudaError = true;

  // Allocate and Initialize the timescale for each cell
  rt_float const InfiniteTime = 1e20;

  rt_float tau_ion_min, tau_cool_min;
  thrust::device_vector<rt_float>::iterator tau_ion_min_ptr;
  thrust::device_vector<rt_float>::iterator tau_cool_min_ptr;
  unsigned int cool_index, ion_index;
  // Find timescale Tau of each cell //
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size
  // Always need to get the cooling time
  thrust::device_vector<rt_float> tau_cool(Data_dev.NumCells, InfiniteTime);
  setGridAndBlockSize(
      Data_dev.NumCells, (void *)fMT_findTauCool_kernel, &gridSize, &blockSize);
  fMT_findTauCool_kernel<<<gridSize, blockSize>>>(
      Constants, Data_dev, Rates_dev, tau_cool.data().get());
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
  tau_cool_min = *thrust::min_element(tau_cool.begin(), tau_cool.end());
  tau_cool_min *= Constants.Timestep_Factor;
  if (!time->updateIonizationTimescale) {
    time->dt_RT = tau_cool_min;
    if (time->current_time + time->dt_RT >= time->t_nextIonizationRate) {
      time->dt_RT = time->t_nextIonizationRate - time->current_time;
      time->updateIonizationTimescale = true;
    }
    imposeDtLimits(Constants, time);
    return;
  }
  // If we need to find the ionization time scale
  thrust::device_vector<rt_float> tau_ion(Data_dev.NumCells, InfiniteTime);
  setGridAndBlockSize(
      Data_dev.NumCells, (void *)fMT_findTauIon_kernel, &gridSize, &blockSize);
  fMT_findTauIon_kernel<<<gridSize, blockSize>>>(
      Constants, Data_dev, Rates_dev, tau_ion.data().get());
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);

  tau_ion_min = *thrust::min_element(tau_ion.begin(), tau_ion.end());
  tau_ion_min *= Constants.Timestep_Factor;
  // ion_index = thrust::distance(tau_ion.begin(), tau_ion_min_ptr);

#if 0
  if (time->current_time >= time->t_nextIonizationRate) {
    printf("Min t_ion %e, min t_cool %e\n", tau_ion_min, tau_cool_min);
    unsigned int id = ion_index;
    if (tau_cool_min < tau_ion_min) id = cool_index;
    float f_H1, L, G, Entropy, T, Density, I_H1, a_H1, n_H1_equilib;

    cudaMemcpy(
        &f_H1, &Data_dev.f_H1[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(&T, &Data_dev.T[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(
        &Density, &Data_dev.Density[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(&n_H1_equilib,
               &Rates_dev.n_HI_Equilibrium[id],
               sizeof(float),
               cudaMemcpyDeviceToHost);

    cudaMemcpy(
        &Entropy, &Data_dev.Entropy[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(&G, &Rates_dev.G[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(&L, &Rates_dev.L[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(
        &I_H1, &Rates_dev.I_H1[id], sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(
        &a_H1, &Rates_dev.alpha_H1[id], sizeof(float), cudaMemcpyDeviceToHost);

    printf(
        "id=%d; f_H1=%8.3e; T=%8.3e; G=%8.3e; L=%8.3e; Density=%8.3e; "
        "I_H1=%8.3e, a_H1=%8.3e; n_H1_equilib=%8.3e\n",
        id,
        f_H1,
        T,
        G,
        L,
        Density,
        I_H1,
        a_H1,
        n_H1_equilib);
  }
#endif

  if (tau_ion_min * 0.5 < tau_cool_min) {  //'normal timestep'
    time->dt_RT = std::min(tau_ion_min, tau_cool_min);
    /* If we calculated Ionization Rates this timestep.
       Ionization flag is already true so don't need to reset it*/
    time->t_nextIonizationRate = time->current_time + time->dt_RT;
  } else {
    time->dt_RT = tau_cool_min;
    time->updateIonizationTimescale = false;
    time->t_nextIonizationRate = time->current_time + tau_ion_min;
  }
  imposeDtLimits(Constants, time);
  return;
}
