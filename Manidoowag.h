#ifndef _MANIDOOWAG_H_
#define _MANIDOOWAG_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "RT.h"
double getTimeInSeconds();

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

void calculateRecombinationRatesInclCooling(rt_float const ExpansionFactor,
                                            RT_ConstantsT const Constants_dev,
                                            RT_Data const Data_dev,
                                            /* out */ RT_RatesBlockT Rates_dev);

rt_float findMinimumTimescale(RT_ConstantsT const Constants,
                              RT_RatesBlockT Rates_dev,
                              RT_Data const Data_dev);

void findNextTimestep(size_t const nParticles,
                      rt_float const t_stop,
                      rt_float const t,
                      RT_Data const Data_dev,
                      RT_RatesBlockT const Rates_dev,
                      RT_ConstantsT const Constants,
                      /* Output */
                      rt_float &dt_RT,
                      unsigned char &LastIterationFlag);

void findNextTimestep_CUDA(size_t const nParticles,
                           RT_Data const Data_dev,
                           RT_RatesBlockT const Rates_dev,
                           RT_ConstantsT const Constants,
                           rt_time_t *time);

bool isOutputTime(rt_float const t_outputTimeUnits,
                  rt_float const *const OutputTimes,
                  size_t const nOutputTimes,
                  size_t const OutputTimeIndex);

void minmaxOfVector(rt_float *Vector_dev,
                    size_t num_els,
                    /* out */
                    rt_float *min,
                    rt_float *max);

void MultipleSourcesFromSingle(size_t const nSources,
                               /* Modified */
                               RT_SourceT *Source);

size_t readHaloCatalogue(char *const HaloCatalogueFile,
                         rt_float *&haloCentres,
                         rt_float *&Ls);

size_t readOutputTimes(
    char const *const OutputTimesFile,
    rt_float &outputTimesUnit, /* To convert outputTimes[i] to s */
    rt_float *&outputTimes);

void setVectorToValue(rt_float *const Vector,
                      rt_float const Value,
                      size_t const count);

/* Note: if the current time (t_Ma) is in the output list, it will be skipped!
 * This should be called after isOutputTime().
 */
size_t skipMissedOutputTimes(rt_float const t_outputTimeUnits,
                             rt_float const *const OutputTimes,
                             size_t const nOutputTimes,
                             size_t *const OutputTimeIndex);

void updateCumulativeHeatingRate(RT_RatesBlockT Rates);

void updateParticles(const rt_float dt_RT,
                     const rt_float Redshift,
                     RT_ConstantsT const Constants,
                     RT_RatesBlockT const Rates,
                     /* Updated */ RT_Data Data);

void zeroRates(RT_RatesBlockT Rates_dev);

void zeroCoolingRates(RT_RatesBlockT Rates_dev);

void collisionalIonizationRate(RT_ConstantsT const Constants,
                               RT_Data Data_dev,
                               /*Updated*/ RT_RatesBlockT const Rates_dev);

#endif /* __cplusplus */

#endif