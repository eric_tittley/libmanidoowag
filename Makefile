include ../make.config
# Expected definitions:
#  DEFS
#  INCS_BASE, INC_MPI
#  SERIAL, MPI, and OPENMP (1 or 0)
#  AR, AR_FLAGS
#  LD, LD_FLAGS
#  DEPEND, DEPEND_FLAGS

DEFS    += -DHAVE_CONFIG
DEFINES += -DHAVE_CONFIG

INCS = $(INCS_BASE) \
       $(INC_LIBCUDASUPPORT) \
       $(INC_RT) \
       $(INC_RT_CUDA) \
       $(NVCC_INC)

SRC_C = getTimeInSeconds.c

SRC_CPP = findNextTimestep.cpp \
          isOutputTime.cpp  \
          MultipleSourcesFromSingle.cpp \
          readHaloCatalogue.cpp \
          readOutputTimes.cpp \
          skipMissedOutputTimes.cpp

SRC_CUDA = calculateRecombinationRatesInclCooling.cu \
           collisionalIonizationRate.cu \
           findMinimumTimescale.cu \
           findNextTimestep_CUDA.cu \
           minmaxOfVector.cu \
           setVectorToValue.cu \
           updateCumulativeHeatingRate.cu \
           updateParticles.cu \
           zeroRates.cu
           
OBJS_CPP  = $(SRC_CPP:.cpp=.o)
OBJS_C    = $(SRC_C:.c=.o)
OBJS_CUDA = $(SRC_CUDA:.cu=.o)

LIBRARY = libManidoowag.a

all: $(LIBRARY)

$(LIBRARY): $(OBJS_C) $(OBJS_CPP) $(OBJS_CUDA)
	$(AR) $(AR_FLAGS) r $(LIBRARY) $(OBJS_C) $(OBJS_CPP) $(OBJS_CUDA)

clean:
	-rm *.o
	-rm *~
	-rm link.o
	-rm -fr tmp

distclean: clean
	-rm $(LIBRARY)

indent:
	indent *.cpp

.SUFFIXES: .o .c .cpp .cu

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<

.cpp.o:
	$(CXX) $(CXXFLAGS) $(DEFS) $(INCS) -c $<

.cu.o:
	$(NVCC) $(NVCCFLAGS) $(DEFS) $(INCS) -dc $<
